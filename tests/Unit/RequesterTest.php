<?php

namespace Tests\Unit;

use App\Classes\Requester;
use Tests\TestCase;
use Mockery;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RequesterTest extends TestCase
{
    use RefreshDatabase;

    public function mockMakeRequestResponse()
    {
        return [
            'rates' => [
                'CAD' => 1.40
            ],
            'base'  => 'USD',
            'date'  => '2020-05-23',
        ];
    }

    public function test_response()
    {
        $currencies = [
            'from' => 'USD',
            'to' => 'CAD',
        ];

        $mock = Mockery::mock(Requester::class, [$currencies])
            ->makePartial();

        $mock->shouldReceive('makeRequest')
            ->andReturn($this->mockMakeRequestResponse());

        $this->assertEquals([
            'cache' => false,
            'from' => 'USD',
            'to' => 'CAD',
            'rate' => 1.40
        ], $mock->response());

        $this->assertEquals([
            'cache' => true,
            'from' => 'USD',
            'to' => 'CAD',
            'rate' => 1.40
        ], $mock->response());
    }
}
