<?php

namespace Tests\Unit;

use App\Classes\Cacher;
use App\Rate;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CacherTest extends TestCase
{
    use RefreshDatabase;

    private $cache;
    private $payload;

    public function setUp(): void
    {
        parent::setUp();

        $this->cache = new Cacher();
        $this->payload = [
            'from' => 'CAD',
            'to' => 'USD',
            'rate' => 0.98
        ];
    }

    public function test_put_method()
    {
        $this->cache->put($this->payload);
        $this->assertDatabaseHas('rates', $this->payload);
    }

    public function test_get_with_from_to()
    {
        $this->cache->put($this->payload);

        $response = $this->cache
            ->get([
                'from' => 'CAD',
                'to' => 'USD',
            ])
            ->toArray();

        $this->assertEquals($this->payload, $response);
    }

    public function test_get_with_to_from()
    {
        $this->cache->put($this->payload);

        $response = $this->cache
            ->get([
                'from' => 'USD',
                'to' => 'CAD',
            ])
            ->toArray();

        $this->assertEquals($this->payload, $response);
    }

    public function test_purge()
    {
        $rate = factory(Rate::class)->create();

        $this->cache->purge();

        $this->assertDatabaseMissing('rates', $rate->toArray());
    }

    public function test_purge_expired()
    {
        // purgeExpired
        Model::unguard();

        $payload = [
            [
                'from' => 'CAD',
                'to' => 'USD',
                'rate' => 1.40984785234,
                'created_at' => now()->subMinutes(config('cache.validity') + 10)
            ],
            [
                'from' => 'JP',
                'to' => 'USD',
                'rate' => 77.40984785234,
                'created_at' => now()
            ],
        ];

        Rate::insert($payload);

        $this->cache->purgeExpired();

        $this->assertDatabaseCount('rates', 1)
            ->assertDatabaseHas('rates', $payload[1]);
    }
}
