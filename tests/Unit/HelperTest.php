<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class HelperTest extends TestCase
{
    private $precision;

    protected function setUp(): void
    {
        parent::setUp();
        $this->precision = config('rates.precision');
    }

    public function test_round_precision()
    {
        $amount = 123.533984578894;

        $this->assertEquals(round_precision($amount), round($amount, $this->precision));
    }
}
