<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Http\Middleware\UppercaseCurrencyMiddleware;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class UpprcaseCurrencyMiddlewareTest extends TestCase
{
    /** @test */
    public function param_currencies_are_always_in_uppcase()
    {
        $request = new Request([], [], [], [], [], ['REQUEST_URI' => '/100/cad/usd']);

        $request->setRouteResolver(function () use ($request) {
            return (new Route('GET', '{amount}/{from}/{to}', []))
                ->bind($request);
        });

        $middleware = new UppercaseCurrencyMiddleware();

        $middleware->handle($request, function ($req) {
            $this->assertEquals('CAD', $req->from);
            $this->assertEquals('USD', $req->to);
        });
    }
}
