<?php

namespace Tests\Feature;

use Tests\TestCase;

class ShowInfoTest extends TestCase
{
    /**
     * @test
     *
     * Test info API
     *
     * @return void
     */
    public function a_client_should_see_api_info()
    {
        $this->getJson('/api/exchange/info')
            ->assertSuccessful()
            ->assertExactJson([
                'error' => 0,
                'msg' => 'API written by Allan Joseph Cagadas'
            ]);
    }
}
