<?php

namespace Tests\Feature;

use App\Classes\Requester;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetConversionTest extends TestCase
{
    use RefreshDatabase;

    public function mockUnCachedResponse($inverted = false)
    {
        return [
            'cache' => false,
            'from' => 'CAD',
            'to' => 'USD',
            'rate' => !$inverted ? 0.7139396320 : 1/0.7139396320
        ];
    }

    public function mockCachedResponse($inverted = false)
    {
        return [
            'cache' => true,
            'from' => 'CAD',
            'to' => 'USD',
            'rate' => !$inverted ? 0.7139396320 : 1/0.7139396320
        ];
    }

    /** @test */
    public function a_successful_response_when_currencies_are_supported()
    {
        $this->withoutExceptionHandling();
        $mock = $this->partialMock(Requester::class);
        $mock->shouldReceive('response')
            ->twice()
            ->andReturn($this->mockUnCachedResponse(), $this->mockCachedResponse());

        $this->getJson('/api/exchange/100/CAD/USD')
            ->assertSuccessful()
            ->assertJsonFragment([
                'error' => 0,
                'fromCache' => 0,
                'amount' => round_precision($this->mockUnCachedResponse()['rate'] * 100)
            ]);

        $this->getJson('/api/exchange/100/CAD/USD')
            ->assertSuccessful()
            ->assertJsonFragment([
                'error' => 0,
                'fromCache' => 1,
                'amount' => round_precision($this->mockCachedResponse()['rate'] * 100)
        ]);
    }

    /** @test */
    public function a_successful_response_when_currencies_are_supported_and_inverted()
    {
        $this->withoutExceptionHandling();
        $mock = $this->partialMock(Requester::class);
        $mock->shouldReceive('response')
            ->twice()
            ->andReturn($this->mockUnCachedResponse(true), $this->mockCachedResponse(true));

        $this->getJson('/api/exchange/100/USD/CAD')
            ->assertSuccessful()
            ->assertJsonFragment([
                'error' => 0,
                'fromCache' => 0,
                'amount' => round_precision($this->mockUnCachedResponse()['rate'] * 100)
            ]);

        $this->getJson('/api/exchange/100/USD/CAD')
            ->assertSuccessful()
            ->assertJsonFragment([
                'error' => 0,
                'fromCache' => 1,
                'amount' => round_precision($this->mockCachedResponse()['rate'] * 100)
        ]);
    }
}
