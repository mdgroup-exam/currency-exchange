<?php

namespace Tests\Feature;

use Tests\TestCase;

class FallbackResponseest extends TestCase
{
   /**
     * @test
     *
     * Test Fallback response API
     *
     * @return void
     */
    public function an_invalid_request_should_return_a_defalt_response()
    {
        $this->getJson('/api/random')
            ->assertStatus(404)
            ->assertExactJson([
                'error' => 1,
                'msg' => 'invalid request'
            ]);
    }
}
