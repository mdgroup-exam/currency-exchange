# About

This is using the Laravel framework

# Installation

- Download or clone the repository & `cd <project path>`
- Run `composer install`
- Run `cp .env.example .env`
- Edit the database configuration
- Run `php artisan key:generate`
- Run `php artisan migrate`
- Run `php artisan serve --port=3000`
- Open http://localhost:3000 on your browser

# Running Test

- Run `php artisan test --env=testing`

# Running Scheduler

- Run `php artisan schedule:run`
- Or setup cron
    - Run `crontab -e`
    - add `* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1` where `path-to-your-project` must be updated

# Notes

- Frontend has been slightly modified because it was not handling the response properly
- Postman: https://www.postman.com/collections/89ce854c2c9ca0e87143