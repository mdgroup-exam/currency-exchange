<?php

return [
    /**
     * List of currencies supported by the system
     */
    'supported' => explode(',', env('SUPPORTED_CURRENCIES', 'CAD,JPY,USD,GBP,EUR,RUB,HKD,CHF')),

    /**
     * Precision of converted currency.
     * Number of decimal places.
     */
    'precision' => env('PRECISION', 2),

    /**
     * exchange rate api
     */
    'api' => env('EXCHANGE_API', 'https://api.exchangeratesapi.io/latest')

];
