<?php

namespace App\Providers;

use App\Classes\Requester;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class RequesterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Requester::class, function ($app) {
            $request = $app->make(Request::class);
            return new Requester([
                'from' => $request->from,
                'to' => $request->to
            ]);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
