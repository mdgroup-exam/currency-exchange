<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Rate extends Model
{
    /**
     * Don't use default timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    protected $hidden = ['created_at'];

    protected $fillable = ['from', 'to', 'rate'];

    protected $dates = ['created_at'];


    /**
     * Set the precision of rate
     *
     * @param  string  $value
     * @return void
     */
    public function setRateAttribute($value)
    {
        $this->attributes['rate'] = $value;
    }

    /**
     * Scope a query to filter entry with either of the two columns
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  Array  $args
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEitherOrFromTo($query, $args)
    {
        return $query->where(function($query) use ($args) {
                return $query->where('from', $args['from'])
                    ->where('to', $args['to']);
            })
            ->orWhere(function($query) use ($args) {
                return $query->where('to', $args['from'])
                    ->where('from', $args['to']);
            });
    }

    public function scopeIsValid($query)
    {
        return $query->where('created_at', '>=', now()->subMinutes(Config::get('cache.validity'))->format('Y-m-d H:i:s'));
    }

    public function scopeIsExpired($query)
    {
        return $query->where('created_at', '<', now()->subMinutes(Config::get('cache.validity'))->format('Y-m-d H:i:s'));
    }
}
