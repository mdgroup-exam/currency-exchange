<?php

/**
 * Round off amount based on system requirements
 *
 * @param Float $amount
 * @return Float
 */
if (!function_exists('round_precision')) {
    function round_precision($amount) {
        return round($amount, config('rates.precision'));
    }
}
