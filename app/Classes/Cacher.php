<?php

namespace App\Classes;

use App\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class Cacher
{
    /**
     * Check database if request rates are available
     *
     * @param Array $args
     * @return void
     */
    public function get(Array $args)
    {
        return Rate::select('from', 'to', 'rate')
            ->eitherOrFromTo($args)
            ->isValid()
            ->first();
    }

    /**
     * Store data
     *
     * @param Array $args
     * @return void
     */
    public function put(Array $args)
    {
        return Rate::create($args);
    }

    /**
     * Delete all database entry
     *
     * @return void
     */
    public function purge()
    {
        return Rate::truncate();
    }

    public function purgeExpired()
    {
        return Rate::isExpired()->delete();
    }
}
