<?php

namespace App\Classes;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use App\Classes\Cacher;

class Requester
{
    private $api;

    private $args;

    private $cacher;

    /**
     * Set cache properties
     *
     * @param Array $args
     * @param Cacher $cacher
     */
    public function __construct(Array $args, Cacher $cacher = null)
    {
        $this->api = Config::get('rates.api');
        $this->args = $args;
        $this->cacher = $cacher ?? new Cacher;
    }

    /**
     * Make request to exchange rate api
     *
     * @return json
     */
    public function makeRequest() : Array
    {
        $response = Http::get($this->api, [
            'base' => $this->args['from'],
            'symbols' => $this->args['to']
        ]);

        return $response->json();
    }

    /**
     * Get response from DB/Cache or fetch frm API then store result
     *
     * @return Array
     */
    public function response() : Array
    {
        $cache = $this->cacher->get($this->args);

        if ($cache) {
            return array_merge($cache->toArray(), ['cache' => true]);
        }

        $response = $this->makeRequest($this->args);

        $payload = [
            'from' => $response['base'],
            'to' => $this->args['to'],
            'rate' => $response['rates'][$this->args['to']]
        ];

        $this->cacher->put($payload);

        return array_merge($payload, ['cache' => false]);
    }
}
