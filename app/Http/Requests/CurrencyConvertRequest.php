<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

class CurrencyConvertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currencyRule = [
            'required',
            Rule::in(Config::get('rates.supported'))
        ];

        return [
            'amount' => 'required|integer|min:1',
            'from' => $currencyRule,
            'to' => $currencyRule
        ];
    }

    public function messages()
    {
        return [
            'from.in' => 'currency code :input not supported',
            'to.in' => 'currency code :input not supported',
        ];
    }
}
