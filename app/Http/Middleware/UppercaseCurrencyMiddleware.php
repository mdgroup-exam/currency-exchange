<?php

namespace App\Http\Middleware;

use Closure;

class UppercaseCurrencyMiddleware
{
    /**
     * Handle an incoming request.
     * Converts parameters to request
     * Capitalize currencies
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->merge([
            'from' => strtoupper($request->route('from')),
            'to' => strtoupper($request->route('to')),
            'amount' => $request->route('amount')
        ]);

        return $next($request);
    }
}
