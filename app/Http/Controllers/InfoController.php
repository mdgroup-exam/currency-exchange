<?php

namespace App\Http\Controllers;

class InfoController extends Controller
{
    /**
     * Returns the API information
     *
     * @return void
     */
    public function show()
    {
        return response()->json([
            'error' => 0,
            'msg' => 'API written by Allan Joseph Cagadas'
        ]);
    }
}
