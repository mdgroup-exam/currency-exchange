<?php

namespace App\Http\Controllers;

use App\Classes\Cacher;

class RateController extends Controller
{
    public function destroy(Cacher $cacher)
    {
        $cacher->purge();

        return response()->json([
                'error' => 0,
                'msg' => "OK"
            ]);
    }
}
