<?php

namespace App\Http\Controllers;

use App\Classes\Requester;
use App\Http\Requests\CurrencyConvertRequest;

class ExchangeController extends Controller
{
    /**
     * Convert requested amount to a currency
     *
     * @param CurrencyConvertRequest $request
     * @return void
     */
    public function convert(CurrencyConvertRequest $request, Requester $requester)
    {
        $response = $requester->response();

        $rate = $response['from'] == $request->from ? $response['rate'] : 1/$response['rate'];

        return response()->json([
                'error' => 0,
                'fromCache' => $response['cache'] ? 1 : 0,
                'amount' => round_precision($rate * $request->amount)
        ]);
    }
}
