<?php

use Illuminate\Support\Facades\Route;

Route::prefix('exchange')->group(function () {
    Route::get('info', 'InfoController@show');
    Route::get('{amount}/{from}/{to}', 'ExchangeController@convert')
        ->middleware('uppercase.currency');
});

Route::get('cache/clear', 'RateController@destroy');


Route::fallback(function () {
    return response()->json([
        'error' => 1,
        'msg' => 'invalid request'
    ], 404);
});
