<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Rate;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Config;

$factory->define(Rate::class, function (Faker $faker) {
    $currencies = Config::get('rates.supported');

    $from = array_rand($currencies, 1);
    $to = array_rand(
        array_filter($currencies, function($currency) use ($from) {
            return $currency != $from;
        })
    );

    return [
        'from' => $from,
        'to' => $to,
        'rate' => $faker->randomFloat(8, 0.1, 2)
    ];
});
